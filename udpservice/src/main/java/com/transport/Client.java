
package com.transport;

import com.transport.beans.DefaultACKMessage;
import com.transport.beans.PacketMessage;
import com.transport.beans.PingMessage;
import com.transport.util.PacketHelper;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static com.transport.util.Log.DEBUG;
import static com.transport.util.Log.ERROR;
import static com.transport.util.Log.INFO;
import static com.transport.util.Log.TRACE;
import static com.transport.util.Log.debug;
import static com.transport.util.Log.error;
import static com.transport.util.Log.info;
import static com.transport.util.Log.trace;

public abstract class Client extends ConnectionWrapper implements EndPoint {
    static {
        try {
            // Needed for NIO selectors on Android 2.2.
            System.setProperty("java.net.preferIPv6Addresses", "false");
        } catch (AccessControlException ignored) {
        }
    }

    private final Serialization serialization;
    private Selector selector;
    private Thread updateThread;
    private volatile boolean udpRegistered;
    private final Object updateLock = new Object();
    private final Object reconnectLock = new Object();
    private Object udpRegistrationLock = new Object();
    private boolean isClosed;
    private int emptySelects;
    private volatile boolean shutdown = true;
    private volatile boolean isReconnecting = true;
    //重连后需要重发的数据包
    public List<PacketMessage> resendPackets = Collections.synchronizedList(new ArrayList<PacketMessage>());

    /**
     * Creates a Client with a write buffer size of 8192 and an object buffer size of 2048.
     */
    public Client() {
        this(8192, 2048);
    }

    public Client(int writeBufferSize, int objectBufferSize) {
        this(writeBufferSize, objectBufferSize, new PacketSerialization());
    }

    public Client(int writeBufferSize, int objectBufferSize, Serialization serialization) {
        super();
        endPoint = this;

        this.serialization = serialization;

        initialize(serialization, writeBufferSize, objectBufferSize);

        try {
            selector = Selector.open();
        } catch (IOException ex) {
            throw new RuntimeException("Error opening selector.", ex);
        }
    }


    public Serialization getSerialization() {
        return serialization;
    }

    /**
     * Opens a udp only client.
     */
    public void connect(String host, int udpPort) throws IOException {
        connect(InetAddress.getByName(host), udpPort);
    }

    public void connect(InetAddress host, int udpPort) throws IOException {
        if (host == null) throw new IllegalArgumentException("host cannot be null.");
        if (udpPort <= 0) throw new IllegalArgumentException("udpPort is invalid.");
        connect(new InetSocketAddress(host, udpPort));
    }

    public void connect(InetSocketAddress udpAddress) throws IOException {
        if (udpAddress == null) throw new IllegalArgumentException("host cannot be null.");
        if (Thread.currentThread() == getUpdateThread())
            throw new IllegalStateException("Cannot connect on the connection's update thread.");
        close();
        try {
            long endTime;
            synchronized (updateLock) {
                udpRegistered = false;
                selector.wakeup();
                endTime = System.currentTimeMillis() + 5000;//建立连接5秒超时
                connect(selector, udpAddress);
            }

            // Wait for RegisterUDP reply.
            synchronized (udpRegistrationLock) {
                while (!udpRegistered && System.currentTimeMillis() < endTime) {
                    PingMessage pingMessage = new PingMessage(PacketHelper.PingMessage);
                    //如果不是重发的包 需要自增加流水号，否则使用原来的流水号
                    if (serialNumber == -1)
                        serialNumber = 0;
                    pingMessage.serialNumber = serialNumber++;
                    udp.send(pingMessage, udpAddress);
                    try {
                        udpRegistrationLock.wait(1000);
                    } catch (InterruptedException ignored) {
                    }
                }
                if (!udpRegistered)
                    throw new SocketTimeoutException("Connected, but timed out during UDP registration: " + udpAddress);
            }
            if (resendPackets.size() > 0) {
                for (PacketMessage packet : resendPackets) {
                    sendObject(packet, true);
                }
                resendPackets.clear();
            }
        } catch (IOException ex) {
            close();
            if (INFO) info("transport", this + " connectedFailed.");
            notifyConnectFailed();
            throw ex;
        } finally {
            isReconnecting = false;
        }
    }


    /**
     * 线程安全
     */
    public void reconnect() {
        synchronized (updateLock) { // Blocks to avoid a select while the selector is used to bind the server connection.
        }
        synchronized (reconnectLock) {
            if (!isReconnecting) {
                isReconnecting = true;
                //首先保存需要重发的数据包
                for (PacketMessage packet : packetsWaitForACK) {
                    if (packet.getMessageID() != PacketHelper.PingMessage) {
                        resendPackets.add(packet);
                        packet.sendTimes = 0;
                    }
                }
                packetsWaitForACK.clear();
                stop();
                //通知上层发起重连 上层可能会需要重新获取UDP服务器地址等其他操作后再发起重连
                notifyReConnect();
            }
        }
    }

    /**
     * Reads or writes any pending data for this client. Multiple threads should not call this method at the same time.
     *
     * @param timeout Wait for up to the specified milliseconds for data to be ready to process. May be zero to return immediately
     *                if there is no data to process.
     */
    public void update(int timeout) throws IOException {
        updateThread = Thread.currentThread();
        synchronized (updateLock) { // Blocks to avoid a select while the selector is used to bind the server connection.
        }
        long startTime = System.currentTimeMillis();
        int select;
        if (timeout > 0) {
            select = selector.select(timeout);
        } else {
            select = selector.selectNow();
        }
        if (select == 0) {
            emptySelects++;
            if (emptySelects == 100) {
                emptySelects = 0;
                // NIO freaks and returns immediately with 0 sometimes, so try to keep from hogging the CPU.
                long elapsedTime = System.currentTimeMillis() - startTime;
                try {
                    if (elapsedTime < 25) Thread.sleep(25 - elapsedTime);
                } catch (InterruptedException ex) {
                }
            }
        } else {
            emptySelects = 0;
            isClosed = false;
            Set<SelectionKey> keys = selector.selectedKeys();
            synchronized (keys) {
                for (Iterator<SelectionKey> iter = keys.iterator(); iter.hasNext(); ) {
                    SelectionKey selectionKey = iter.next();
                    iter.remove();
                    try {
                        if (selectionKey.isReadable()) {
                            if (udp == null || readFromAddress() == null) continue;
                            PacketMessage object = readObject();
                            if (object == null) continue;
                            if (!isConnected && !udpRegistered) {
                                synchronized (udpRegistrationLock) {
                                    udpRegistered = true;
                                    udpRegistrationLock.notifyAll();
                                    if (TRACE)
                                        trace("kryonet", this + " received UDP: RegisterUDP");
                                    if (DEBUG) {
                                        debug("kryonet", "Port " + udp.datagramChannel.socket().getLocalPort()
                                                + "/UDP connected to: " + udp.connectedAddress);
                                    }
                                    setConnected(true);
                                }
                                notifyConnected();
                            }
                            if (DEBUG) {
                                String objectString = object == null ? "null" : object.getClass().getSimpleName();
                                debug("transport", this + " received UDP: " + objectString + "\n" + object.toString());
                            }
                            if (object instanceof DefaultACKMessage) {
                                //通用处理 从等待ACK列表中清除
                                PacketMessage tmp = findPacketBySerialNumber(((DefaultACKMessage) object).ackNo);
                                if (tmp != null) {
                                    packetsWaitForACK.remove(tmp);
                                }
                                //只有下面的ACK包需要通知前台UI处理
//                                if (((DefaultACKMessage) object).ackID == PacketHelper.OrderCompleteMessage) {
//                                    if (tmp != null) {
//                                        ((DefaultACKMessage) object).orderID = ((OrderCompleteMessage) tmp).getOrderID();
//                                        notifyReceived(object);
//                                    }
//                                }

                            } else {
                                //首先需要发送ACK包
                                sendObjectACK(object);
                                //处理数据
                                notifyReceived(object);
                            }
                        }
                    } catch (CancelledKeyException ignored) {
                        // ConnectionWrapper is closed.
                    }
                }
            }
        }
        if (isConnected && !isReconnecting) {
            List<PacketMessage> packetsNeedResend = new ArrayList<>();
            long time = System.currentTimeMillis();
            //检查等待ack的数据包数组
            for (PacketMessage packetMessage : packetsWaitForACK) {
                if (packetMessage.isUponMaxTimes() && packetMessage.isTimeOut(time)) {
                    //发现一个超出重传次数的消息包 我们认为链路出现问题 重连 同时清除无需重新发送的数据包
                    debug("transport", "发现一个超出重传次数的消息包 我们认为链路出现问题 重连 同时清除无需重新发送的数据包");
                    reconnect();
                    return;
                }
                if (packetMessage.isTimeOut(time))
                    packetsNeedResend.add(packetMessage);
            }

            for (PacketMessage packetMessage : packetsNeedResend) {
                packetsWaitForACK.remove(packetMessage);
                sendObject(packetMessage, true);
            }
            keepAlive();
        }
    }

    void keepAlive() {
        if (!isConnected || isReconnecting) return;
        long time = System.currentTimeMillis();
        if (udp != null && needsKeepAlive(time)) {
            PingMessage pingMessage = new PingMessage(PacketHelper.PingMessage);
            sendObject(pingMessage);
        }
    }

    /**
     * 开始接受消息
     */
    public void run() {
        if (TRACE) trace("transport", "Client thread started.");
        shutdown = false;
        while (!shutdown) {
            try {
                update(250);
            } catch (IOException ex) {
                if (TRACE) {
                    if (isConnected)
                        trace("transport", "Unable to update connection: " + this, ex);
                    else
                        trace("transport", "Unable to update connection.", ex);
                } else if (DEBUG) {
                    if (isConnected)
                        debug("transport", this + " update: " + ex.getMessage());
                    else
                        debug("transport", "Unable to update connection: " + ex.getMessage());
                }
                reconnect();
            } catch (Exception ex) {
                lastProtocolError = ex;
                if (ERROR) {
                    if (isConnected)
                        error("transport", "Error updating connection: " + this, ex);
                    else
                        error("transport", "Error updating connection.", ex);
                }
                reconnect();
                throw ex;
            }
        }
        if (TRACE) trace("transport", "Client thread stopped.");
    }

    public void start() {
        // Try to let any previous update thread stop.
        if (updateThread != null) {
            shutdown = true;
            try {
                updateThread.join(5000);
            } catch (InterruptedException ignored) {
            }
        }
        updateThread = new Thread(this, "Client");
        updateThread.setDaemon(true);
        updateThread.start();
    }

    public void stop() {
        if (shutdown) return;
        close();
        if (TRACE) trace("transport", "Client thread stopping.");
        shutdown = true;
        selector.wakeup();
    }

    public void close() {
        super.close();
        synchronized (updateLock) { // Blocks to avoid a select while the selector is used to bind the server connection.
        }
        // Select one last time to complete closing the socket.
        if (!isClosed) {
            isClosed = true;
            selector.wakeup();
            try {
                selector.selectNow();
            } catch (IOException ignored) {
            }
        }
    }

    /**
     * Releases the resources used by this client, which may no longer be used.
     */
    public void dispose() throws IOException {
        close();
        selector.close();
    }

    public void addListener(Listener listener) {
        super.addListener(listener);
        if (TRACE) trace("transport", "Client listener added.");
    }

    public void removeListener(Listener listener) {
        super.removeListener(listener);
        if (TRACE) trace("transport", "Client listener removed.");
    }

    /**
     * 设置心跳间隔时间，
     */
    public void setKeepAliveUDP(int keepAliveMillis) {
        if (udp == null) throw new IllegalStateException("Not connected via UDP.");
        setKeepAliveMillis(keepAliveMillis);
    }

    public Thread getUpdateThread() {
        return updateThread;
    }

}
