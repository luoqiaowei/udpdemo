package com.transport.util;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by luwei on 16/5/31.
 */
public class ByteUtil {

    /**
     * 将包装好的字节转译 [0x1a]转成[0x1a, 0x01] ; [0x1e]转成[0x1a, 0x02]，,并在头尾加上标识符0x1e
     *
     * @param data
     * @return
     */
    public static byte[] encryptJT808Bytes(byte[] data) {
        java.nio.ByteBuffer bf = java.nio.ByteBuffer.allocate(data.length * 2 + 2);
        bf.put((byte) 0x1E);
        for (int i = 0; i < data.length; i++) {
            if (data[i] == 0x1A) {
                bf.put((byte) 0x1A);
                bf.put((byte) 0x01);
            } else if (data[i] == 0x1E) {
                bf.put((byte) 0x1A);
                bf.put((byte) 0x02);
            } else {
                bf.put(data[i]);
            }
        }
        bf.put((byte) 0x1e);

        byte[] ret = new byte[bf.position()];
        System.arraycopy(bf.array(), 0, ret, 0, ret.length);
        return ret;
    }

    /**
     * 去掉头尾的标识符，并将字节组转译 [0x1a, 0x01]转成[0x1a] ; [0x1a, 0x02]转成[0x1e]
     *
     * @param data
     * @return
     */
    public static byte[] decryptJT808Bytes(byte[] data) {
        java.nio.ByteBuffer bf = java.nio.ByteBuffer.allocate(data.length);

        for (int i = 1; i < data.length - 1; i++) {
            if (i == data.length - 2) {
                bf.put(data[i]);
                break;
            }

            if (data[i] == (byte) 0x1A && data[i + 1] == (byte) 0x01) {
                bf.put((byte) 0x1A);
                i++;
            } else if (data[i] == (byte) 0x1A && data[i + 1] == (byte) 0x02) {
                bf.put((byte) 0x1E);
                i++;
            } else {
                bf.put(data[i]);
            }

        }

        byte[] b = new byte[bf.position()];
        System.arraycopy(bf.array(), 0, b, 0, b.length);
        return b;
    }


    /**
     * @功能: BCD码转为10进制串(阿拉伯数据)
     * @参数: BCD码
     * @结果: 10进制串
     */
    public static String bcd2Str(byte[] bytes) {
        return bcd2Str(bytes, 0, bytes.length);
    }

    public static String bcd2Str(byte[] bytes, int offset, int length) {
        if (bytes.length < offset || bytes.length - offset < length)
            return null;
        StringBuffer temp = new StringBuffer(length * 2);
        for (int i = offset; i < offset + length; i++) {
            byte tmp = bytes[i];
            temp.append((byte) ((tmp & 0xf0) >>> 4));
            temp.append((byte) (tmp & 0x0f));
        }
        return temp.toString();
    }

    /**
     * @功能: 10进制串转为BCD码
     * @参数: 10进制串
     * @结果: BCD码
     */
    public static byte[] str2Bcd(String asc) {
        int len = asc.length();
        int mod = len % 2;
        if (mod != 0) {
            asc = "0" + asc;
            len = asc.length();
        }
        byte abt[];
        if (len >= 2) {
            len = len / 2;
        }
        byte bbt[] = new byte[len];
        abt = asc.getBytes();
        int j, k;
        for (int p = 0; p < asc.length() / 2; p++) {
            if ((abt[2 * p] >= '0') && (abt[2 * p] <= '9')) {
                j = abt[2 * p] - '0';
            } else if ((abt[2 * p] >= 'a') && (abt[2 * p] <= 'z')) {
                j = abt[2 * p] - 'a' + 0x0a;
            } else {
                j = abt[2 * p] - 'A' + 0x0a;
            }
            if ((abt[2 * p + 1] >= '0') && (abt[2 * p + 1] <= '9')) {
                k = abt[2 * p + 1] - '0';
            } else if ((abt[2 * p + 1] >= 'a') && (abt[2 * p + 1] <= 'z')) {
                k = abt[2 * p + 1] - 'a' + 0x0a;
            } else {
                k = abt[2 * p + 1] - 'A' + 0x0a;
            }
            int a = (j << 4) + k;
            byte b = (byte) a;
            bbt[p] = b;
        }
        return bbt;
    }

    private static byte Byte2BCD(byte paramByte) {
        return (byte) ((paramByte >> 4 & 0xF) * 10 + (paramByte & 0xF));
    }

    private static byte BCD2Byte(byte paramByte) {
        return (byte) ((paramByte / 10 << 4 & 0xF0) + (paramByte % 10 & 0xF));
    }


    public static byte[] intToBytes(int integer) {
        return ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(integer).array();
    }


    public static byte[] shortToBytes(short value) {
        return ByteBuffer.allocate(2).order(ByteOrder.BIG_ENDIAN).putShort(value).array();
    }

    public static int bytesToInt(byte[] b, int start, int length) {
        return ByteBuffer.wrap(b, start, length).order(ByteOrder.BIG_ENDIAN).getInt();
    }

    public static int bytesToInt(byte[] b, int start) {
        return bytesToInt(b, start, 4);
    }

    public static int bytesToInt(byte[] b) {
        return bytesToInt(b, 0);
    }

    public static short bytesToShort(byte[] b, int offset, int length) {
        return ByteBuffer.wrap(b, offset, length).order(ByteOrder.BIG_ENDIAN).getShort();
    }

    public static short bytesToShort(byte[] b, int offset) {
        return bytesToShort(b, offset, 2);
    }

    public static short bytesToShort(byte[] b) {
        return bytesToShort(b, 0);
    }


    public static byte encrypt(byte[] bytes) {
        return encrypt(bytes, bytes.length);
    }

    public static byte encrypt(byte[] bytes, int length) {
        byte tmp = bytes[0];
        for (int i = 1; i < length; i++) {
            tmp = (byte) (tmp ^ bytes[i]);
        }
        return tmp;
    }


    /**
     * Convert byte[] to hex string.这里我们可以将byte转换成int，然后利用Integer.toHexString(int)
     * 来转换成16进制字符串。
     *
     * @param src byte[] data
     * @return hex string
     */
    public static String bytesToHexString(byte[] src) {
        return bytesToHexString(src, 0, src.length);
    }

    public static String bytesToHexString(byte[] src, int offset, int length) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0 || src.length <= offset) {
            return null;
        }
        for (int i = offset; i < length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    public static byte[] split(byte[] data, int offset, int length) {
        byte[] tmp = new byte[length];
        int tmpIndex = 0;
        int dataIndex = offset;
        for (; (tmpIndex < length && dataIndex < data.length); tmpIndex++, dataIndex++) {
            tmp[tmpIndex] = data[dataIndex];
        }
        return tmp;
    }


    /**
     * @功能:测试用例
     * @参数: 参数
     */
    public static void main(String[] args) {
        byte[] b = "3512505758".getBytes();
        System.out.println(bytesToHexString(b, 0, b.length));
        byte[] tmp = split(b, 4, b.length);
        System.out.println(bytesToHexString(tmp, 0, tmp.length));
    }

}
