package com.transport.beans;

import com.transport.util.ByteUtil;
import com.transport.util.PacketHelper;

/**
 * 中心下发的通用应答 以及 APP端的通用应答
 * <p/>
 * Created by luwei on 16/6/1.
 */

// TODO: 16/6/3 测试与服务器通信
public class DefaultACKMessage extends PacketMessage {

    public short ackNo;//应答流水号
    public short ackID;//应答ID
    public byte result;//结果
    public String reason;//原因

    public DefaultACKMessage(short messageID) {
        super(messageID);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("ackNo:" + ackNo + "\n");
        sb.append("ackID: " + ackID + "\n");
        sb.append("result: " + result + "\n");
        sb.append("reason: " + reason + "\n");
        return sb.toString();
    }

    /**
     * 这个应该是app端回复中心端才会使用
     *
     * @return
     */
    @Override
    public byte[] getPacketBody() {
        byte[] body = new byte[5 + (PacketHelper.isEmpty(reason) ? 0 : reason.getBytes().length)];
        //保存流水号
        System.arraycopy(ByteUtil.shortToBytes(ackNo), 0, body, 0, 2);
        //保存应答ID
        System.arraycopy(ByteUtil.shortToBytes(ackID), 0, body, 2, 2);
        //保存结果
        body[4] = result;
        //如果有原因 保存原因 APP通用应答无原因字段  但是中心应答会有原因字段
        if (!PacketHelper.isEmpty(reason)) {
            System.arraycopy(reason.getBytes(), 0, body, 5, reason.getBytes().length);
        }
        return body;
    }

    /**
     * 这个是中心端的通用答复包解包
     *
     * @param data 完整的UDP数据包
     */
    @Override
    public void writePacketBody(byte[] data) {
        //读取应答流水号
        ackNo = ByteUtil.bytesToShort(data, headSize);
        //读取应答ID
        ackID = ByteUtil.bytesToShort(data, headSize + 2);
        result = data[headSize + 4];
        if (bodySize > 5)
            reason = new String(data, headSize + 5, bodySize - 5);
    }
}
