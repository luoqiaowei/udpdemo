/* Copyright (c) 2008, Nathan Sweet
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following
 * conditions are met:
 * 
 * - Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided with the distribution.
 * - Neither the name of Esoteric Software nor the names of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

package com.transport;

import com.transport.beans.PacketMessage;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.util.Set;

import static com.transport.util.Log.DEBUG;
import static com.transport.util.Log.ERROR;
import static com.transport.util.Log.INFO;
import static com.transport.util.Log.TRACE;
import static com.transport.util.Log.WARN;
import static com.transport.util.Log.debug;
import static com.transport.util.Log.error;
import static com.transport.util.Log.info;
import static com.transport.util.Log.trace;
import static com.transport.util.Log.warn;


/**
 * Manages TCP and optionally UDP connections from many {@link Client Clients}.
 *
 * @author Nathan Sweet <misc@n4te.com>
 */
public abstract class Server implements EndPoint {
    private final Serialization serialization;
    private final int writeBufferSize, objectBufferSize;
    private final Selector selector;
    private int emptySelects;
    private UDPConnection udp;
    private ConnectionWrapper currentConnection;
    Listener[] listeners = {};
    private Object listenerLock = new Object();
    private int nextConnectionID = 1;
    private volatile boolean shutdown;
    private Object updateLock = new Object();
    private Thread updateThread;

    private Listener dispatchListener = new Listener() {
        public void connected(ConnectionWrapper connectionWrapper) {
            Listener[] listeners = Server.this.listeners;
            for (int i = 0, n = listeners.length; i < n; i++)
                listeners[i].connected(connectionWrapper);
        }

        public void disconnected(ConnectionWrapper connectionWrapper) {
            removeConnection(connectionWrapper);
            Listener[] listeners = Server.this.listeners;
            for (int i = 0, n = listeners.length; i < n; i++)
                listeners[i].disconnected(connectionWrapper);
        }

        public void received(ConnectionWrapper connectionWrapper, PacketMessage object) {
            Listener[] listeners = Server.this.listeners;
            for (int i = 0, n = listeners.length; i < n; i++)
                listeners[i].received(connectionWrapper, object);
        }

    };

    /**
     * Creates a Server with a write buffer size of 16384 and an object buffer size of 2048.
     */
    public Server() {
        this(16384, 2048);
    }

    /**
     * @param writeBufferSize  One buffer of this size is allocated for each connected client. Objects are serialized to the write
     *                         buffer where the bytes are queued until they can be written to the TCP socket.
     *                         <p/>
     *                         Normally the socket is writable and the bytes are written immediately. If the socket cannot be written to and
     *                         enough serialized objects are queued to overflow the buffer, then the connection will be closed.
     *                         <p/>
     *                         The write buffer should be sized at least as large as the largest object that will be sent, plus some head room to
     *                         allow for some serialized objects to be queued in case the buffer is temporarily not writable. The amount of head
     *                         room needed is dependent upon the size of objects being sent and how often they are sent.
     * @param objectBufferSize One (using only TCP) or three (using both TCP and UDP) buffers of this size are allocated. These
     *                         buffers are used to hold the bytes for a single object graph until it can be sent over the network or
     *                         deserialized.
     *                         <p/>
     *                         The object buffers should be sized at least as large as the largest object that will be sent or received.
     */
    public Server(int writeBufferSize, int objectBufferSize) {
        this(writeBufferSize, objectBufferSize, new PacketSerialization());
    }

    public Server(int writeBufferSize, int objectBufferSize, Serialization serialization) {
        this.writeBufferSize = writeBufferSize;
        this.objectBufferSize = objectBufferSize;

        this.serialization = serialization;


        try {
            selector = Selector.open();
        } catch (IOException ex) {
            throw new RuntimeException("Error opening selector.", ex);
        }
    }


    public Serialization getSerialization() {
        return serialization;
    }


    /**
     * Opens a udp only server.
     *
     * @throws IOException if the server could not be opened.
     */
    public void bind(int udpPort) throws IOException {
        bind(new InetSocketAddress(InetAddress.getByName("localhost"), udpPort));
    }


    /**
     * @param udpPort May be null.
     */
    public void bind(InetSocketAddress udpPort) throws IOException {
        close();
        synchronized (updateLock) {
            selector.wakeup();
            try {
                if (udpPort != null) {
                    udp = new UDPConnection(serialization, writeBufferSize, objectBufferSize);
                    udp.bind(selector, udpPort);
                    if (DEBUG)
                        debug("kryonet", "Accepting connections on port: " + udpPort + "/UDP");
                }
            } catch (IOException ex) {
                close();
                throw ex;
            }
        }
        if (INFO) info("kryonet", "Server opened.");
    }

    /**
     * Accepts any new connections and reads or writes any pending data for the current connections.
     *
     * @param timeout Wait for up to the specified milliseconds for a connection to be ready to process. May be zero to return
     *                immediately if there are no connections to process.
     */
    public void update(int timeout) throws IOException {
        updateThread = Thread.currentThread();
        synchronized (updateLock) { // Blocks to avoid a select while the selector is used to bind the server connection.
        }
        long startTime = System.currentTimeMillis();
        int select = 0;
        if (timeout > 0) {
            select = selector.select(timeout);
        } else {
            select = selector.selectNow();
        }
        if (select == 0) {
            emptySelects++;
            if (emptySelects == 100) {
                emptySelects = 0;
                // NIO freaks and returns immediately with 0 sometimes, so try to keep from hogging the CPU.
                long elapsedTime = System.currentTimeMillis() - startTime;
                try {
                    if (elapsedTime < 25) Thread.sleep(25 - elapsedTime);
                } catch (InterruptedException ex) {
                }
            }
        } else {
            emptySelects = 0;
            Set<SelectionKey> keys = selector.selectedKeys();
            synchronized (keys) {
                UDPConnection udp = this.udp;
                for (Iterator<SelectionKey> iter = keys.iterator(); iter.hasNext(); ) {
                    SelectionKey selectionKey = iter.next();
                    iter.remove();
                    ConnectionWrapper fromConnectionWrapper = currentConnection;
                    try {
                        // Must be a UDP read operation.
                        if (udp == null) {
                            selectionKey.channel().close();
                            continue;
                        }
                        InetSocketAddress fromAddress;
                        try {
                            fromAddress = udp.readFromAddress();
                            if (fromConnectionWrapper == null || !fromConnectionWrapper.connectedAddress.equals(fromAddress)) {
                                fromConnectionWrapper = currentConnection = acceptOperation(fromAddress);
                            }
                        } catch (IOException ex) {
                            if (WARN) warn("kryonet", "Error reading UDP data.", ex);
                            continue;
                        }
                        if (fromAddress == null) continue;

                        PacketMessage object;
                        try {
                            object = fromConnectionWrapper == null ? udp.readObject() : fromConnectionWrapper.readObject();
                        } catch (RuntimeException ex) {
                            if (WARN) {
                                if (fromConnectionWrapper != null) {
                                    if (ERROR)
                                        error("kryonet", "Error reading UDP from connection: " + fromConnectionWrapper, ex);
                                } else
                                    warn("kryonet", "Error reading UDP from unregistered address: " + fromAddress, ex);
                            }
                            continue;
                        }
                        if (fromConnectionWrapper != null) {
                            if (DEBUG) {
                                String objectString = object == null ? "null" : object.getClass().getSimpleName();
                                debug("kryonet", fromConnectionWrapper + " received UDP: " + objectString);
                            }
                            fromConnectionWrapper.notifyReceived(object);
                            continue;
                        }
                        if (DEBUG)
                            debug("kryonet", "Ignoring UDP from unregistered address: " + fromAddress);
                    } catch (CancelledKeyException ex) {
                        if (fromConnectionWrapper != null)
                            fromConnectionWrapper.close();
                        else
                            selectionKey.channel().close();
                    }
                }
            }
        }
    }


    public void run() {
        if (TRACE) trace("kryonet", "Server thread started.");
        shutdown = false;
        while (!shutdown) {
            try {
                update(250);
            } catch (IOException ex) {
                if (ERROR) error("kryonet", "Error updating server connections.", ex);
                close();
            }
        }
        if (TRACE) trace("kryonet", "Server thread stopped.");
    }

    public void start() {
        new Thread(this, "Server").start();
    }

    public void stop() {
        if (shutdown) return;
        close();
        if (TRACE) trace("kryonet", "Server thread stopping.");
        shutdown = true;
    }

    private ConnectionWrapper acceptOperation(InetSocketAddress fromAddress) {
        ConnectionWrapper connectionWrapper = newConnection();
        connectionWrapper.connectedAddress = fromAddress;
        connectionWrapper.initialize(serialization, writeBufferSize, objectBufferSize);
        connectionWrapper.endPoint = this;
        UDPConnection udp = this.udp;
        if (udp != null) connectionWrapper.udp = udp;
        try {
            int id = nextConnectionID++;
            if (nextConnectionID == -1) nextConnectionID = 1;
            connectionWrapper.setConnected(true);
            connectionWrapper.addListener(dispatchListener);
            addConnection(id, connectionWrapper);


            if (udp == null) connectionWrapper.notifyConnected();
            return connectionWrapper;
        } catch (Exception ex) {
            connectionWrapper.close();
            if (DEBUG) debug("kryonet", "Unable to accept TCP connectionWrapper.", ex);
            return null;
        }
    }

    /**
     * Allows the connections used by the server to be subclassed. This can be useful for storage per connection without an
     * additional lookup.
     */
    protected ConnectionWrapper newConnection() {
        return new ConnectionWrapper();
    }


    private void addConnection(int id, ConnectionWrapper connectionWrapper) {
        currentConnection = connectionWrapper;
    }

    void removeConnection(ConnectionWrapper connectionWrapper) {
//        pendingConnections.remove();
    }


    public void sendToAllUDP(PacketMessage object) {
        currentConnection.sendObject(object);
    }

    public void addListener(Listener listener) {
        if (listener == null) throw new IllegalArgumentException("listener cannot be null.");
        synchronized (listenerLock) {
            Listener[] listeners = this.listeners;
            int n = listeners.length;
            for (int i = 0; i < n; i++)
                if (listener == listeners[i]) return;
            Listener[] newListeners = new Listener[n + 1];
            newListeners[0] = listener;
            System.arraycopy(listeners, 0, newListeners, 1, n);
            this.listeners = newListeners;
        }
        if (TRACE) trace("kryonet", "Server listener added: " + listener.getClass().getName());
    }

    public void removeListener(Listener listener) {
        if (listener == null) throw new IllegalArgumentException("listener cannot be null.");
        synchronized (listenerLock) {
            Listener[] listeners = this.listeners;
            int n = listeners.length;
            Listener[] newListeners = new Listener[n - 1];
            for (int i = 0, ii = 0; i < n; i++) {
                Listener copyListener = listeners[i];
                if (listener == copyListener) continue;
                if (ii == n - 1) return;
                newListeners[ii++] = copyListener;
            }
            this.listeners = newListeners;
        }
        if (TRACE) trace("kryonet", "Server listener removed: " + listener.getClass().getName());
    }

    /**
     * Closes all open connections and the server port(s).
     */
    public void close() {
        if (INFO && currentConnection != null) {
            info("kryonet", "Closing server connections...");
            currentConnection.close();
        }
        UDPConnection udp = this.udp;
        if (udp != null) {
            udp.close();
            this.udp = null;
        }

        synchronized (updateLock) { // Blocks to avoid a select while the selector is used to bind the server connection.
        }
        // Select one last time to complete closing the socket.
        selector.wakeup();
        try {
            selector.selectNow();
        } catch (IOException ignored) {
        }
    }

    /**
     * Releases the resources used by this server, which may no longer be used.
     */
    public void dispose() throws IOException {
        close();
        selector.close();
    }

    public Thread getUpdateThread() {
        return updateThread;
    }


}
