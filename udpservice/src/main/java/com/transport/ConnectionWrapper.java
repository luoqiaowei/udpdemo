
package com.transport;

import com.transport.beans.DefaultACKMessage;
import com.transport.beans.PacketMessage;
import com.transport.util.PacketHelper;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.channels.Selector;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.transport.util.Log.DEBUG;
import static com.transport.util.Log.ERROR;
import static com.transport.util.Log.INFO;
import static com.transport.util.Log.TRACE;
import static com.transport.util.Log.debug;
import static com.transport.util.Log.error;
import static com.transport.util.Log.info;
import static com.transport.util.Log.trace;

public class ConnectionWrapper implements IConnection {
    //提供给外部用来控制udp服务的生命周期
    com.transport.EndPoint endPoint;
    UDPConnection udp;
    InetSocketAddress connectedAddress;
    private com.transport.Listener[] listeners = {};
    private Object listenerLock = new Object();
    volatile boolean isConnected;
    volatile Exception lastProtocolError;
    private int keepAliveMillis = 60000;//心跳一分钟内一次

    private long lastPingTime = 0;//最近一次发送心跳的时间
    //用来保存等待ACK的消息包
    public List<PacketMessage> packetsWaitForACK = Collections.synchronizedList(new ArrayList<PacketMessage>());

    public short serialNumber = 0;

    protected ConnectionWrapper() {
    }

    void initialize(com.transport.Serialization serialization, int writeBufferSize, int objectBufferSize) {
        udp = new UDPConnection(serialization, writeBufferSize, objectBufferSize);
    }

    public PacketMessage findPacketBySerialNumber(int index) {
        for (PacketMessage packet : packetsWaitForACK) {
            if (packet.serialNumber == index)
                return packet;
        }
        return null;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public Exception getLastProtocolError() {
        return lastProtocolError;
    }


    public int sendObjectACK(PacketMessage object) {
        if (object == null) throw new IllegalArgumentException("object cannot be null.");
        if (object.getMessageID() == PacketHelper.Center_DefaultACKMessage)
            throw new IllegalArgumentException("中心通用应答没有ACK.");
        SocketAddress address = connectedAddress;
        if (address == null && isConnected)
            throw new IllegalStateException("ConnectionWrapper is not connected via UDP.");
        DefaultACKMessage objectACK = new DefaultACKMessage(PacketHelper.DefaultACKMessage);
        objectACK.ackID = object.getMessageID();
        objectACK.ackNo = object.serialNumber;
        objectACK.result = 0;
        return sendObject(objectACK);
    }

    public int sendObject(PacketMessage object, boolean isResend) {
        if (object == null) throw new IllegalArgumentException("object cannot be null.");
        SocketAddress address = connectedAddress;
        if (address == null && isConnected)
            throw new IllegalStateException("ConnectionWrapper is not connected via UDP.");

        try {
            if (address == null)
                throw new SocketException("ConnectionWrapper is closed.");
            //如果不是重发的包 需要自增加流水号，否则使用原来的流水号
            if (!isResend) {
                if (serialNumber == -1)
                    serialNumber = 0;
                object.serialNumber = serialNumber++;
            }
            object.sendTimes++;//记录发送次数

            int length = udp.send(object, address);
            //记录最近一次发送的时间
            object.lastSendTime = System.currentTimeMillis();
            //更新心跳包和位置更新包的发送时间
            if (object.getMessageID() == PacketHelper.PingMessage) {
                lastPingTime = object.lastSendTime;
            }
            if (length == 0) {
                if (TRACE) trace("transport", this + " UDP had nothing to send.");
            } else {
                //如果不是发送的通用应答 需要收到ACK
                if (object.getMessageID() != PacketHelper.DefaultACKMessage)
                    packetsWaitForACK.add(object);
                if (DEBUG) {
                    if (length != -1) {
                        String objectString = object == null ? "null" : object.getClass().getSimpleName();
                        if (!(object instanceof PacketMessage)) {
                            debug("transport", this + " sent UDP: " + objectString + " (" + object.serialNumber + ")");
                        } else if (TRACE) {
                            trace("transport", this + " sent UDP: " + objectString + " (" + object.serialNumber + ")");
                        }
                    } else
                        debug("transport", this + " was unable to send, UDP socket buffer full.");
                }
            }
            return length;
        } catch (IOException ex) {
            if (DEBUG) debug("transport", "Unable to send UDP with connection: " + this, ex);
            getEndPoint().reconnect();
            return 0;
        } catch (Exception ex) {
            if (ERROR) error("transport", "Unable to send UDP with connection: " + this, ex);
            getEndPoint().reconnect();
            return 0;
        }
    }

    public int sendObject(PacketMessage object) {
        return sendObject(object, false);
    }

    public PacketMessage readObject() {
        SocketAddress address = connectedAddress;
        if (address == null && isConnected)
            throw new IllegalStateException("ConnectionWrapper is not connected via UDP.");

        try {
            if (address == null)
                throw new SocketException("ConnectionWrapper is closed.");
            PacketMessage object = udp.readObject();
            if (object == null) {
                if (TRACE) trace("transport", this + " UDP had nothing to read.");
            } else if (DEBUG) {
                String objectString = object == null ? "null" : object.getClass().getSimpleName();
                debug("transport", this + " read UDP: " + objectString);
            }
            return object;
        } catch (IOException ex) {
            if (DEBUG) debug("transport", "Unable to send UDP with connection: " + this, ex);
            return null;
        } catch (Exception ex) {
            if (ERROR) error("transport", "Unable to send UDP with connection: " + this, ex);
            return null;
        }
    }

    public void close() {
        boolean wasConnected = isConnected;
        isConnected = false;
        if (udp != null && connectedAddress != null) udp.close();
        if (wasConnected) {
            notifyDisconnected();
            if (INFO) info("transport", this + " disconnected.");
        }
        setConnected(false);
    }

    /**
     * If the listener already exists, it is not added again.
     */
    public void addListener(com.transport.Listener listener) {
        if (listener == null) throw new IllegalArgumentException("listener cannot be null.");
        synchronized (listenerLock) {
            com.transport.Listener[] listeners = this.listeners;
            int n = listeners.length;
            for (int i = 0; i < n; i++)
                if (listener == listeners[i]) return;
            com.transport.Listener[] newListeners = new com.transport.Listener[n + 1];
            newListeners[0] = listener;
            System.arraycopy(listeners, 0, newListeners, 1, n);
            this.listeners = newListeners;
        }
        if (TRACE)
            trace("transport", "ConnectionWrapper listener added: " + listener.getClass().getName());
    }

    public void removeListener(com.transport.Listener listener) {
        if (listener == null) throw new IllegalArgumentException("listener cannot be null.");
        synchronized (listenerLock) {
            com.transport.Listener[] listeners = this.listeners;
            int n = listeners.length;
            if (n == 0) return;
            com.transport.Listener[] newListeners = new com.transport.Listener[n - 1];
            for (int i = 0, ii = 0; i < n; i++) {
                com.transport.Listener copyListener = listeners[i];
                if (listener == copyListener) continue;
                if (ii == n - 1) return;
                newListeners[ii++] = copyListener;
            }
            this.listeners = newListeners;
        }
        if (TRACE)
            trace("kryonet", "ConnectionWrapper listener removed: " + listener.getClass().getName());
    }

    void notifyConnected() {
        com.transport.Listener[] listeners = this.listeners;
        for (int i = 0, n = listeners.length; i < n; i++)
            listeners[i].connected(this);
    }

    void notifyDisconnected() {
        com.transport.Listener[] listeners = this.listeners;
        for (int i = 0, n = listeners.length; i < n; i++)
            listeners[i].disconnected(this);
    }

    void notifyReceived(PacketMessage object) {
        com.transport.Listener[] listeners = this.listeners;
        for (int i = 0, n = listeners.length; i < n; i++)
            listeners[i].received(this, object);
    }

    void notifyReConnect() {
        com.transport.Listener[] listeners = this.listeners;
        for (int i = 0, n = listeners.length; i < n; i++)
            listeners[i].reconnect();
    }

    void notifyConnectFailed() {
        com.transport.Listener[] listeners = this.listeners;
        for (int i = 0, n = listeners.length; i < n; i++)
            listeners[i].connectFailed();
    }


    /**
     * Returns the local {@link com.transport.Client}  to which this connection belongs.
     */
    public com.transport.EndPoint getEndPoint() {
        return endPoint;
    }


    /**
     * Returns the IP address and port of the remote end of the UDP connection, or null if this connection is not connected.
     */
    public InetSocketAddress getRemoteAddressUDP() {
        return connectedAddress;
    }


    /**
     * Returns the number of bytes that are waiting to be written to the UDP socket, if any.
     */
    public int getWriteBufferSize() {
        return udp.writeBuffer.position();
    }


    void setConnected(boolean isConnected) {
        this.isConnected = isConnected;
    }

    /**
     * 判断是否需要重新发送心跳包
     *
     * @param time
     * @return 判断结果
     */
    public boolean needsKeepAlive(long time) {
        return connectedAddress != null && (lastPingTime == 0 || (keepAliveMillis > 0 && time - lastPingTime > keepAliveMillis));
    }


    public void connect(Selector selector, InetSocketAddress remoteAddress) throws IOException {
        if (udp == null) {
            throw new IllegalStateException("You should init a udp connection firstly.");
        }
        connectedAddress = remoteAddress;
        udp.connect(selector, remoteAddress);
    }

    public InetSocketAddress readFromAddress() throws IOException {
        if (udp == null) {
            throw new IllegalStateException("You should init a udp connection firstly.");
        }
        return udp.readFromAddress();
    }

    public void setKeepAliveMillis(int keepAliveMillis) {
        this.keepAliveMillis = keepAliveMillis;
    }

}
