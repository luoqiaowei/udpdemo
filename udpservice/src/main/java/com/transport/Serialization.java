
package com.transport;

import com.transport.beans.PacketMessage;

import java.nio.ByteBuffer;

/**
 * Controls how objects are transmitted over the network.
 */
public interface Serialization {

    void write(ByteBuffer buffer, PacketMessage packet);

    PacketMessage read(ByteBuffer buffer);

    /**
     * The fixed number of bytes that will be written by {@link #writeLength(ByteBuffer, int)} and read by
     * {@link #readLength(ByteBuffer)}.
     */
    int getLengthLength();

    void writeLength(ByteBuffer buffer, int length);

    int readLength(ByteBuffer buffer);
}
