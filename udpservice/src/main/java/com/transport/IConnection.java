package com.transport;

import com.transport.beans.PacketMessage;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.Selector;

/**
 * Created by luwei on 16/5/31.
 */
public interface IConnection {

    public void connect(Selector selector, InetSocketAddress remoteAddress) throws IOException;

    public InetSocketAddress readFromAddress() throws IOException;

    public PacketMessage readObject();

    /**
     * This method is thread safe.
     */
    public int sendObject(PacketMessage object) throws IOException;

    public void close();
}
