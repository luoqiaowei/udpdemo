
package com.transport;

import com.transport.beans.PacketMessage;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;

import static com.transport.util.Log.DEBUG;
import static com.transport.util.Log.debug;
import static com.transport.util.Log.info;

public class UDPConnection {
    InetSocketAddress connectedAddress;
    DatagramChannel datagramChannel;
    final ByteBuffer readBuffer, writeBuffer;
    private final Serialization serialization;
    private SelectionKey selectionKey;
    private final Object writeLock = new Object();


    public UDPConnection(Serialization serialization, int writeBufferSize, int objectBufferSize) {
        this.serialization = serialization;
        readBuffer = ByteBuffer.allocate(objectBufferSize);
        writeBuffer = ByteBuffer.allocateDirect(writeBufferSize);
    }

    public void bind(Selector selector, InetSocketAddress localPort) throws IOException {
        close();
        readBuffer.clear();
        writeBuffer.clear();
        try {
            datagramChannel = selector.provider().openDatagramChannel();
            datagramChannel.socket().bind(localPort);
            datagramChannel.configureBlocking(false);
            selectionKey = datagramChannel.register(selector, SelectionKey.OP_READ);
        } catch (IOException ex) {
            close();
            throw ex;
        }
    }

    public void connect(Selector selector, InetSocketAddress remoteAddress) throws IOException {
        close();
        readBuffer.clear();
        writeBuffer.clear();
        try {
            datagramChannel = selector.provider().openDatagramChannel();
            datagramChannel.configureBlocking(false);
            datagramChannel.socket().bind(null);
            datagramChannel.socket().connect(remoteAddress);
            selectionKey = datagramChannel.register(selector, SelectionKey.OP_READ);
            connectedAddress = remoteAddress;
        } catch (Exception ex) {
            close();
            IOException ioEx = new IOException("Unable to connect to: " + remoteAddress);
            ioEx.initCause(ex);
            throw ioEx;
        }
    }

    public InetSocketAddress readFromAddress() throws IOException {
        DatagramChannel datagramChannel = this.datagramChannel;
        if (datagramChannel == null) throw new SocketException("ConnectionWrapper is closed.");
        if (!datagramChannel.isConnected())
            return (InetSocketAddress) datagramChannel.receive(readBuffer); // always null on Android >= 5.0
        datagramChannel.read(readBuffer);
        return connectedAddress;
    }

    public PacketMessage readObject() {
        readBuffer.flip();
        try {
            try {
                PacketMessage object = serialization.read(readBuffer);
                if (readBuffer.hasRemaining())
                    throw new Exception("Incorrect number of bytes (" + readBuffer.remaining()
                            + " remaining) used to deserialize object: " + object);
                if (DEBUG) info("readObject object : " + object.getClass().getName());
                return object;
            } catch (Exception ex) {
                throw new RuntimeException("Error during deserialization.", ex);
            }
        } finally {
            readBuffer.clear();
        }
    }

    /**
     * This method is thread safe.
     */
    public int send(PacketMessage object, SocketAddress address) throws IOException {
        DatagramChannel datagramChannel = this.datagramChannel;
        if (datagramChannel == null) throw new SocketException("ConnectionWrapper is closed.");
        synchronized (writeLock) {
            try {
                try {
                    if (DEBUG) info("send object : " + object.getClass().getName());
                    serialization.write(writeBuffer, object);
                } catch (Exception ex) {
                    throw new RuntimeException("Error serializing object of type: " + object.getClass().getName(), ex);
                }
                writeBuffer.flip();
                int length = writeBuffer.limit();
                datagramChannel.send(writeBuffer, address);
                boolean wasFullWrite = !writeBuffer.hasRemaining();
                return wasFullWrite ? length : -1;
            } finally {
                writeBuffer.clear();
            }
        }
    }

    public void close() {
        connectedAddress = null;
        try {
            if (datagramChannel != null) {
                datagramChannel.close();
                datagramChannel = null;
                if (selectionKey != null) selectionKey.selector().wakeup();
            }
        } catch (IOException ex) {
            if (DEBUG) debug("transport", "Unable to close UDP connection.", ex);
        }
    }

}
