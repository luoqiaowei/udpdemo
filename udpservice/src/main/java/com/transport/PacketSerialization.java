
package com.transport;

import com.transport.beans.PacketMessage;
import com.transport.io.ByteBufferInputStream;
import com.transport.io.ByteBufferOutputStream;
import com.transport.util.ByteUtil;

import java.io.IOException;
import java.nio.ByteBuffer;

import static com.transport.util.Log.INFO;
import static com.transport.util.Log.info;

public class PacketSerialization implements Serialization {
    //    private final Json json = new Json();
    private final ByteBufferInputStream byteBufferInputStream = new ByteBufferInputStream();
    private final ByteBufferOutputStream byteBufferOutputStream = new ByteBufferOutputStream();
    private boolean logging = true;
    private byte[] logBuffer = {};

    public PacketSerialization() {
    }

    public void write(ByteBuffer buffer, PacketMessage object) {
        byteBufferOutputStream.setByteBuffer(buffer);
        int start = buffer.position();
        try {
            byteBufferOutputStream.write(object.Packet2Bytes());
            byteBufferOutputStream.flush();
        } catch (Exception ex) {
            throw new RuntimeException("Error writing object: " + object, ex);
        }
        if (INFO && logging) {
            int end = buffer.position();
            buffer.position(start);
            buffer.limit(end);
            int length = end - start;
            if (logBuffer.length < length) logBuffer = new byte[length];
            buffer.get(logBuffer, 0, length);
            buffer.position(end);
            buffer.limit(buffer.capacity());
            String message = ByteUtil.bytesToHexString(logBuffer, 0, length);
            info("Wrote: " + message);
        }
    }

    public PacketMessage read(ByteBuffer buffer) {
        byteBufferInputStream.setByteBuffer(buffer);
        try {
            byte[] data = new byte[1024];
            int offset = 0;
            while (true) {
                int length = byteBufferInputStream.read(data, offset, data.length - offset);
                if (length == -1) break;
                if (length == 0) {
                    byte[] newData = new byte[data.length * 2];
                    System.arraycopy(data, 0, newData, 0, data.length);
                    data = newData;
                } else
                    offset += length;
            }
            return parse(data, 0, offset);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public PacketMessage parse(byte[] data, int offset, int length) {
        String message = ByteUtil.bytesToHexString(data, offset, length);
        info("read: " + message);
        try {
            return PacketMessage.bytes2Packet(ByteUtil.split(data, offset, length));
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void writeLength(ByteBuffer buffer, int length) {
        buffer.putInt(length);
    }

    public int readLength(ByteBuffer buffer) {
        return buffer.getInt();
    }

    public int getLengthLength() {
        return 4;
    }
}
