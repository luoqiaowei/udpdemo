package com.transport.util;

import com.transport.beans.DefaultACKMessage;
import com.transport.beans.PacketMessage;
import com.transport.beans.PingMessage;

/**
 * Created by luwei on 16/6/1.
 */
public class PacketHelper {
    //中心下发消息
    public static final short Center_DefaultACKMessage = (short) 0x8001;    //中心下发通用应答

    //驾驶员消息
    public static final short DefaultACKMessage = (short) 0x0001;    //APP通用应答
    public static final short PingMessage = (short) 0x0002;    //APP心跳


    public static PacketMessage getPacketMessage(int messageID) {
        switch (messageID) {
            case Center_DefaultACKMessage:
                return new DefaultACKMessage(Center_DefaultACKMessage);
            case DefaultACKMessage:
                return new DefaultACKMessage(DefaultACKMessage);
            case PingMessage:
                return new PingMessage(PingMessage);
            default:
                return new DefaultACKMessage(Center_DefaultACKMessage);
        }
    }

    /**
     * 判断给定字符串是否空白串。 空白串是指由空格、制表符、回车符、换行符组成的字符串 若输入字符串为null或空字符串，返回true
     *
     * @param input
     * @return boolean
     */
    public static boolean isEmpty(String input) {
        if (input == null || "".equals(input) || "null".equals(input))
            return true;

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c != ' ' && c != '\t' && c != '\r' && c != '\n') {
                return false;
            }
        }
        return true;
    }

}
