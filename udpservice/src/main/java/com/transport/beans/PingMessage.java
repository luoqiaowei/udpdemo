package com.transport.beans;

/**
 * 心跳包
 * 已经通过测试，能够与服务器正常通信
 * Created by luwei on 16/6/1.
 */
public class PingMessage extends PacketMessage {

    private int filter = 0;
    private int bookAvailable;
    private int assistantAvailable;

    public PingMessage(short messageID) {
        super(messageID);
    }


    @Override
    public byte[] getPacketBody() {
        byte[] body = new byte[1];
        body[0] = (byte) filter;
        return body;
    }

    /**
     * app端不会收到心跳包 所以不需要实现
     *
     * @param data 完整的UDP数据包
     */
    @Override
    public void writePacketBody(byte[] data) {
    }

    public int getAssistantAvailable() {
        return assistantAvailable;
    }

    public void setAssistantAvailable(int assistantAvailable) {
        this.assistantAvailable = assistantAvailable;
        int tmp = filter & 1;//首先保存0位bit
        filter = (assistantAvailable << 1) | tmp;
    }

    public int getBookAvailable() {
        return bookAvailable;
    }

    public void setBookAvailable(int bookAvailable) {
        this.bookAvailable = bookAvailable;
        int tmp = filter & 2;//首先保存1位bit
        filter = bookAvailable | tmp;
    }
}