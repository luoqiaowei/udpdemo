
package com.transport.example.position;

import com.transport.Client;
import com.transport.util.Log;

import java.io.IOException;
import java.util.HashMap;

import javax.swing.JOptionPane;


public class PositionClient {
    UI ui;
    Client client;
    String port;

    public PositionClient() {
        client = new Client() {
        };
        client.start();


        // ThreadedListener runs the listener methods on a different thread.
        client.addListener(new com.transport.Listener.ThreadedListener(new com.transport.Listener() {
            public void connected(com.transport.ConnectionWrapper connectionWrapper) {
            }

            public void received(com.transport.ConnectionWrapper connectionWrapper, Object object) {

            }

            public void disconnected(com.transport.ConnectionWrapper connectionWrapper) {
                System.exit(0);
            }
        }));

        ui = new UI();

        String host = ui.inputHost();
        port = ui.inputPort();

        try {
            client.connect(host, Integer.parseInt(port));
            // Server communication after connection can go here, or in Listener#connected().
        } catch (IOException ex) {
            ex.printStackTrace();
        }


    }

    static class UI {
        HashMap<Integer, Character> characters = new HashMap();

        public String inputHost() {
            String input = (String) JOptionPane.showInputDialog(null, "Host:", "Connect to server", JOptionPane.QUESTION_MESSAGE,
                    null, null, "58.213.107.82");
            if (input == null || input.trim().length() == 0) System.exit(1);
            return input.trim();
        }

        public String inputPort() {
            String input = (String) JOptionPane.showInputDialog(null, "Port:", "Connect to server", JOptionPane.QUESTION_MESSAGE,
                    null, null, "5906");
            if (input == null || input.trim().length() == 0) System.exit(1);
            return input.trim();
        }

    }

    public static void main(String[] args) {
        Log.set(Log.LEVEL_DEBUG);
        new PositionClient();
    }
}
