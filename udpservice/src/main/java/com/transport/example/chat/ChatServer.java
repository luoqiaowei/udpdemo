
package com.transport.example.chat;


import com.transport.ConnectionWrapper;
import com.transport.Listener;
import com.transport.Server;
import com.transport.beans.DefaultACKMessage;
import com.transport.beans.PacketMessage;
import com.transport.util.Log;
import com.transport.util.PacketHelper;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class ChatServer {
    Server server;
    static int packetNumber = 0;

    public ChatServer() throws IOException {
        server = new Server() {

            @Override
            public void reconnect() {

            }
        };

        server.addListener(new Listener() {
            public void received(ConnectionWrapper c, PacketMessage object) {
                if ((packetNumber++) % 8 == 0) {
                    if (object == null)
                        throw new IllegalArgumentException("object cannot be null.");
                    if (object.getMessageID() == PacketHelper.DefaultACKMessage) return;
                    DefaultACKMessage objectACK = new DefaultACKMessage(PacketHelper.Center_DefaultACKMessage);
                    objectACK.ackID = object.getMessageID();
                    objectACK.ackNo = object.serialNumber;
                    objectACK.result = 0;
                    c.sendObject(objectACK);
                }
            }

            public void disconnected(ConnectionWrapper c) {

            }
        });
        server.bind(54555);
        server.start();

        // Open a window to provide an easy way to stop the server.
        JFrame frame = new JFrame("Chat Server");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosed(WindowEvent evt) {
                server.stop();
            }
        });
        frame.getContentPane().add(new JLabel("Close to stop the chat server."));
        frame.setSize(320, 200);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }


    // This holds per connection state.
    static class ChatConnection extends ConnectionWrapper {
        public String name;
    }

    public static void main(String[] args) throws IOException {
        Log.set(Log.LEVEL_DEBUG);
        new ChatServer();
    }
}
