
package com.transport.beans;

import com.transport.util.ByteUtil;
import com.transport.util.Log;
import com.transport.util.PacketHelper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import static com.transport.util.Log.INFO;
import static com.transport.util.Log.info;

public abstract class PacketMessage {

    public short serialNumber = 0;//流水号
    public int sendTimes = 0;//发送次数
    public long lastSendTime;//最近一次发送的时间
    public int timeoutMillis = 19000;

    protected short messageID = 0;

    protected short bodySize;

    protected int headSize = 12;//目前是固定的

    private int MaxResendTimes = 3;//最大发送次数


    /**
     * 默认的时间格式
     */
    public static final ThreadLocal<SimpleDateFormat> dateFormater = new ThreadLocal<SimpleDateFormat>() {
        @Override
        protected SimpleDateFormat initialValue() {
            return new SimpleDateFormat("yyMMddHHmmss", Locale.CHINA);
        }
    };

    /**
     * TN+1 = TN ×（N+1）式中：
     * TN+1——每次重传后的应答超时时间；
     * TN——前一次的应答超时时间；
     * N——重传次数。
     *
     * @return 超时时间
     */
    private int getTimeoutMillis() {
        int tmp = timeoutMillis;
        int resendTimes = (sendTimes > 0 ? sendTimes - 1 : 0);
        for (int i = 0; i < resendTimes; i++) {
            tmp = tmp * (i + 1);
        }
        return tmp;
    }

    /**
     * ack接收是否超时
     *
     * @param currentTime
     * @return
     */
    public boolean isTimeOut(long currentTime) {
        return currentTime - lastSendTime > getTimeoutMillis();
    }

    /**
     * 是否达到了重传次数上线
     *
     * @return
     */
    public boolean isUponMaxTimes() {
        return sendTimes >= getMaxResendTimes();
    }

    public PacketMessage(short messageID) {
        this.messageID = messageID;
    }

    /**
     * 从PacketMessage对象转化为Byte[] 用于UDP装包
     *
     * @return Byte[]
     */
    public abstract byte[] getPacketBody();

    /**
     * 把Byte[]数据还原回PacketMessage的属性值 用于UDP解包
     *
     * @param data 完整的UDP数据包
     */
    public abstract void writePacketBody(byte[] data);


    public byte[] Packet2Bytes() {
        //首先创建消息包
        byte[] packetBody = getPacketBody();
        //创建消息头 固定12字节
        byte[] header = new byte[headSize];
        //消息ID 0~1
        System.arraycopy(ByteUtil.shortToBytes(messageID), 0, header, 0, 2);
        //消息体属性 2~3
        short bodySize = (short) (packetBody == null ? 0 : packetBody.length);//2个字节
        //消息体流水号 10~11
        System.arraycopy(ByteUtil.shortToBytes(serialNumber), 0, header, 10, 2);

        //下面填充消息体
        byte[] content = new byte[(bodySize + headSize + 1)];//组成：消息头+消息体+检验码
        //填充消息头
        System.arraycopy(header, 0, content, 0, headSize);
        //填充消息体
        if (packetBody != null)
            System.arraycopy(packetBody, 0, content, headSize, bodySize);
        //生成检验码
        byte checkByte = ByteUtil.encrypt(content, bodySize + headSize);
        content[bodySize + headSize] = checkByte;
        //使用自定义规则转译加密
        return ByteUtil.encryptJT808Bytes(content);
    }

    public static PacketMessage bytes2Packet(byte[] data) throws IOException {
        //首先转译
        byte[] tmp = ByteUtil.decryptJT808Bytes(data);
        //取出校验码 检验
        byte checkByte = tmp[tmp.length - 1];
        byte realCheckByte = ByteUtil.encrypt(tmp, tmp.length - 1);
        if (realCheckByte == checkByte) {
            //读取消息头 首先读取消息ID
            short messageID = ByteUtil.bytesToShort(tmp);
            PacketMessage packetMessage = PacketHelper.getPacketMessage(messageID);
            //读取消息体属性
            packetMessage.bodySize = ByteUtil.bytesToShort(tmp, 2);

            //读取消息体流水号
            packetMessage.serialNumber = ByteUtil.bytesToShort(tmp, 10);
            //读取消息体
            packetMessage.writePacketBody(tmp);
            return packetMessage;
        } else {
            throw new RuntimeException("校验码不一致，数据包已经损坏");
        }
    }

    public short getMessageID() {
        return messageID;
    }

    public void setMessageID(short messageID) {
        this.messageID = messageID;
    }

    public int getMaxResendTimes() {
        return MaxResendTimes;
    }

    public void setMaxResendTimes(int maxResendTimes) {
        MaxResendTimes = maxResendTimes;
    }

    /**
     * 测试单元
     *
     * @param args
     */
    public static void main(String[] args) {
        Log.set(Log.LEVEL_DEBUG);
        PacketMessage packetMessage = PacketHelper.getPacketMessage(PacketHelper.PingMessage);

        byte[] tmp = packetMessage.Packet2Bytes();
        if (INFO) {
            String message = ByteUtil.bytesToHexString(tmp, 0, tmp.length);
            info("Wrote: " + message);
        }
        try {
            PacketMessage pa = PacketMessage.bytes2Packet(tmp);
            info("PacketMessage: " + pa.getClass().getSimpleName());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
